﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BridgeMonitor.Models
{
    public class Fermeture
    {
        [JsonProperty("boat_name")]
        public string NomBateau { get; set; }

        [JsonProperty("closing_type")]
        public string TypeFermeture { get; set; }

        [JsonProperty("closing_date")]
        public DateTime DateFermeture { get; set; }

        [JsonProperty("reopening_date")]
        public DateTime DateRouverture { get; set; }
    }
}
