﻿using BridgeMonitor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace BridgeMonitor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var fermetures = Trouver_Fermeture();
            return View(fermetures);
        }

        public IActionResult Liste_Fermetures()
        {
            var fermetures = Trouver_Fermeture();
            return View(fermetures);
        }

        public IActionResult Detail(string date)
        {
            var fermetures = Trouver_Fermeture();
            List<Fermeture> detail_fermeture = new List<Fermeture>();
            foreach (var fermeture in fermetures)
            {
                if (fermeture.DateFermeture.ToString("yyyyMMddHHmmss") == date)
                {
                    detail_fermeture.Add(fermeture);
                }
            }
            return View(detail_fermeture);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private static readonly HttpClient client = new HttpClient();

        public static List<Fermeture> Trouver_Fermeture()
        {
            var url = client.GetStringAsync("https://api.alexandredubois.com/pont-chaban/api.php");
            var donnee_json = url.Result;
            var resultat = JsonConvert.DeserializeObject<List<Fermeture>>(donnee_json);
            var resultat_trier = resultat.OrderBy(Fermeture => Fermeture.DateFermeture).ToList();
            return resultat_trier;
        }

        public void Calendrier()
        {
            CultureInfo fr = CultureInfo.GetCultureInfo("fr-FR");
            var fermetures = Trouver_Fermeture();
            StringBuilder ecriture = new StringBuilder();
            ecriture.AppendLine("BEGIN:VCALENDAR");
            ecriture.AppendLine("VERSION:2.0");
            ecriture.AppendLine("CALSCALE:GREGORIAN");
            ecriture.AppendLine("METHOD:PUBLISH");
            foreach (var fermeture in fermetures)
            {
                ecriture.AppendLine("BEGIN:VEVENT");
                ecriture.AppendLine("DTSTART:" + fermeture.DateFermeture.ToString("yyyyMMddTHHmm00"));
                ecriture.AppendLine("DTEND:" + fermeture.DateRouverture.ToString("yyyyMMddTHHmm00"));
                if (fermeture.NomBateau == "MAINTENANCE" || fermeture.NomBateau == "VIGILANCE CRUE")
                {
                    ecriture.AppendLine("SUMMARY:Fermeture pour cause de " + fermeture.NomBateau + ".");
                    ecriture.AppendLine("DESCRIPTION:Le pont Chaban Delmas est fermé à la circulation pour cause de " + fermeture.NomBateau + " du " +
                        fermeture.DateFermeture.ToString("dddd dd MMMM yyyy 'à' HH':'mm", fr) + " au " + fermeture.DateRouverture.ToString("dddd dd MMMM yyyy 'à' HH':'mm", fr) + ".");
                } else
                {
                    ecriture.AppendLine("SUMMARY:Fermeture à cause du passage du bateau " + fermeture.NomBateau + ".");
                    ecriture.AppendLine("DESCRIPTION:Le pont Chaban Delmas est fermé à la circulation à cause du passage du bateau " + fermeture.NomBateau + " du " +
                        fermeture.DateFermeture.ToString("dddd dd MMMM yyyy 'à' HH':'mm", fr) + " au " + fermeture.DateRouverture.ToString("dddd dd MMMM yyyy 'à' HH':'mm", fr) + ".");
                }
                ecriture.AppendLine("LOCATION:Pont Jacques Chaban Delmas, 33300 Bordeaux");
                ecriture.AppendLine("END:VEVENT");
            }
            ecriture.AppendLine("END:VCALENDAR");
            string calendrier = ecriture.ToString();
            Response.Headers.Clear();
            Response.ContentType = "text/calendar";
            Response.Headers.Add("content-length", calendrier.Length.ToString());
            Response.Headers.Add("content-disposition", "attachment; filename=\"calendrier.ics\"");
            var bytes = Encoding.UTF8.GetBytes(calendrier);
            Response.Body.WriteAsync(bytes);
            Response.Body.Flush();
        }
    }
}
